import http from 'k6/http';

export default function() {
    const url = 'https://reqres.in/api/users';
    const payload = JSON.stringify({
        name: 'Yayang Tri Wijaya',
        job: 'QA Engineer',
    });

    http.post(url, payload);
}